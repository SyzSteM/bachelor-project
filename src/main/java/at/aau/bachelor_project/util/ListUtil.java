package at.aau.bachelor_project.util;

import java.util.List;
import java.util.Objects;

public class ListUtil {

  private ListUtil() {
    // private constructor to hide default constructor
    // this class should never be instantiated
  }

  public static String prettyToString(List<?> list) {
    var stringBuilder = new StringBuilder();

    for (Object item : list) {
      stringBuilder.append(item);
      stringBuilder.append('\n');
    }

    return stringBuilder.substring(0, stringBuilder.length() - 1);
  }

  public static String prettyToString(List<?> list, int indent) {
    if (Objects.isNull(list) || list.isEmpty()) {
      return "\t".repeat(Math.max(0, indent)) + "empty list";
    }

    var stringBuilder = new StringBuilder();

    for (var item : list) {
      stringBuilder.append("\t".repeat(Math.max(0, indent)));
      stringBuilder.append(item);
      stringBuilder.append('\n');
    }

    return stringBuilder.substring(0, stringBuilder.length() - 1);
  }

}
