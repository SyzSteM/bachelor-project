package at.aau.bachelor_project.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class XMLCounter {

  @JacksonXmlProperty(localName = "type", isAttribute = true)
  private String type;

  @JacksonXmlProperty(localName = "missed", isAttribute = true)
  private String missed;

  @JacksonXmlProperty(localName = "covered", isAttribute = true)
  private String covered;

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getMissed() {
    return missed;
  }

  public void setMissed(String missed) {
    this.missed = missed;
  }

  public String getCovered() {
    return covered;
  }

  public void setCovered(String covered) {
    this.covered = covered;
  }

  @Override
  public String toString() {
    return String.format("counter type '%s', missed '%s', covered '%s'", type, missed, covered);
  }

}
