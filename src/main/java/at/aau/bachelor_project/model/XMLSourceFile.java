package at.aau.bachelor_project.model;

import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import at.aau.bachelor_project.util.ListUtil;

public class XMLSourceFile {

  @JacksonXmlProperty(localName = "name", isAttribute = true)
  private String name;

  @JacksonXmlElementWrapper(useWrapping = false)
  @JacksonXmlProperty(localName = "line")
  private List<XMLLine> lines;

  @JacksonXmlElementWrapper(useWrapping = false)
  @JacksonXmlProperty(localName = "counter")
  private List<XMLCounter> counters;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<XMLLine> getLines() {
    return lines;
  }

  public void setLines(List<XMLLine> lines) {
    this.lines = lines;
  }

  public List<XMLCounter> getCounters() {
    return counters;
  }

  public void setCounters(List<XMLCounter> counters) {
    this.counters = counters;
  }

  @Override
  public String toString() {
    return String.format("""
            source file name '%s'
            \t\tsource file lines:
            %s
            \t\tsource file counters:
            %s""",
        name, ListUtil.prettyToString(lines, 3), ListUtil.prettyToString(counters, 3)
    );
  }

}
