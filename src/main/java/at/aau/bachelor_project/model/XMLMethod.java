package at.aau.bachelor_project.model;

import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import at.aau.bachelor_project.util.ListUtil;

public class XMLMethod {

  @JacksonXmlProperty(localName = "name", isAttribute = true)
  private String name;

  @JacksonXmlElementWrapper(useWrapping = false)
  @JacksonXmlProperty(localName = "counter")
  private List<XMLCounter> counters;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<XMLCounter> getCounters() {
    return counters;
  }

  public void setCounters(List<XMLCounter> counters) {
    this.counters = counters;
  }

  @Override
  public String toString() {
    return String.format("""
        method name '%s'
        \t\t\tmethod counters:
        %s""", name, ListUtil.prettyToString(counters, 4));
  }

}
