package at.aau.bachelor_project.model;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import at.aau.bachelor_project.util.ListUtil;

@JacksonXmlRootElement(localName = "report")
public class XMLReport implements Serializable {

  @JacksonXmlProperty(localName = "name", isAttribute = true)
  private String name;

  @JacksonXmlElementWrapper(useWrapping = false)
  @JacksonXmlProperty(localName = "package")
  private List<XMLPackage> packages;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<XMLPackage> getPackages() {
    return packages;
  }

  public void setPackages(List<XMLPackage> packages) {
    this.packages = packages;
  }

  @Override
  public String toString() {
    return String.format("""
            report name '%s'
            packages:
            %s""",
        name, ListUtil.prettyToString(packages)
    );
  }

}
