package at.aau.bachelor_project.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "RBCS_DATA")
public class MethodEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "method_entity_seq_gen")
  @SequenceGenerator(
      name = "method_entity_seq_gen",
      sequenceName = "seq_method_entity",
      initialValue = 100,
      allocationSize = 1
  )
  @Column(name = "ID", nullable = false)
  private Long id;

  @Column(name = "PACKAGE_NAME")
  private String packageName;

  @Column(name = "CLASS_NAME")
  private String className;

  @Column(name = "METHOD_NAME")
  private String methodName;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getPackageName() {
    return packageName;
  }

  public void setPackageName(String packageName) {
    this.packageName = packageName;
  }

  public String getClassName() {
    return className;
  }

  public void setClassName(String className) {
    this.className = className;
  }

  public String getMethodName() {
    return methodName;
  }

  public void setMethodName(String methodName) {
    this.methodName = methodName;
  }

}
