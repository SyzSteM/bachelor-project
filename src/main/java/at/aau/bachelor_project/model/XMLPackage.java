package at.aau.bachelor_project.model;

import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import at.aau.bachelor_project.util.ListUtil;

public class XMLPackage {

  @JacksonXmlProperty(localName = "name", isAttribute = true)
  private String name;

  @JacksonXmlElementWrapper(useWrapping = false)
  @JacksonXmlProperty(localName = "class")
  private List<XMLClass> classes;

  @JacksonXmlElementWrapper(useWrapping = false)
  @JacksonXmlProperty(localName = "sourcefile")
  private List<XMLSourceFile> sourceFiles;

  @JacksonXmlElementWrapper(useWrapping = false)
  @JacksonXmlProperty(localName = "counter")
  private List<XMLCounter> counters;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<XMLClass> getClasses() {
    return classes;
  }

  public void setClasses(List<XMLClass> classes) {
    this.classes = classes;
  }

  public List<XMLSourceFile> getSourceFiles() {
    return sourceFiles;
  }

  public void setSourceFiles(List<XMLSourceFile> sourceFiles) {
    this.sourceFiles = sourceFiles;
  }

  public List<XMLCounter> getCounters() {
    return counters;
  }

  public void setCounters(List<XMLCounter> counters) {
    this.counters = counters;
  }

  @Override
  public String toString() {
    return String.format("""
            \tpackage name '%s'
            \tpackage classes:
            %s
            \tpackage source files:
            %s
            \tpackage counters:
            %s%n""",
        name, ListUtil.prettyToString(classes, 2), ListUtil.prettyToString(sourceFiles, 2),
        ListUtil.prettyToString(counters, 2)
    );
  }

}
