package at.aau.bachelor_project.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class XMLLine {

  @JacksonXmlProperty(localName = "nr", isAttribute = true)
  private String nr;

  @JacksonXmlProperty(localName = "mi", isAttribute = true)
  private String mi;

  @JacksonXmlProperty(localName = "ci", isAttribute = true)
  private String ci;

  @JacksonXmlProperty(localName = "mb", isAttribute = true)
  private String mb;

  @JacksonXmlProperty(localName = "cb", isAttribute = true)
  private String cb;

  public String getNr() {
    return nr;
  }

  public void setNr(String nr) {
    this.nr = nr;
  }

  public String getMi() {
    return mi;
  }

  public void setMi(String mi) {
    this.mi = mi;
  }

  public String getCi() {
    return ci;
  }

  public void setCi(String ci) {
    this.ci = ci;
  }

  public String getMb() {
    return mb;
  }

  public void setMb(String mb) {
    this.mb = mb;
  }

  public String getCb() {
    return cb;
  }

  public void setCb(String cb) {
    this.cb = cb;
  }

  @Override
  public String toString() {
    return String.format("line nr '%s', mi '%s', ci '%s', mb '%s', cb '%s'", nr, mi, ci, mb, cb);
  }

}
