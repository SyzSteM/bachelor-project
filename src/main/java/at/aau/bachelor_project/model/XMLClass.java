package at.aau.bachelor_project.model;

import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import at.aau.bachelor_project.util.ListUtil;

public class XMLClass {

  @JacksonXmlProperty(localName = "name", isAttribute = true)
  private String name;

  @JacksonXmlProperty(localName = "sourcefilename", isAttribute = true)
  private String sourceFileName;

  @JacksonXmlElementWrapper(useWrapping = false)
  @JacksonXmlProperty(localName = "method")
  private List<XMLMethod> methods;

  @JacksonXmlElementWrapper(useWrapping = false)
  @JacksonXmlProperty(localName = "counter")
  private List<XMLCounter> counters;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSourceFileName() {
    return sourceFileName;
  }

  public void setSourceFileName(String sourceFileName) {
    this.sourceFileName = sourceFileName;
  }

  public List<XMLMethod> getMethods() {
    return methods;
  }

  public void setMethods(List<XMLMethod> methods) {
    this.methods = methods;
  }

  public List<XMLCounter> getCounters() {
    return counters;
  }

  public void setCounters(List<XMLCounter> counters) {
    this.counters = counters;
  }

  @Override
  public String toString() {
    return String.format("""
            class name '%s', sourceFileName '%s'
            \t\tclass methods:
            %s
            \t\tclass counters:
            %s""",
        name, sourceFileName, ListUtil.prettyToString(methods, 3), ListUtil.prettyToString(counters, 3)
    );
  }

}
