package at.aau.bachelor_project.service;

import static javax.swing.UIManager.getColor;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import org.jacoco.core.analysis.Analyzer;
import org.jacoco.core.analysis.CoverageBuilder;
import org.jacoco.core.analysis.IClassCoverage;
import org.jacoco.core.analysis.ICounter;
import org.jacoco.core.data.ExecutionDataStore;
import org.junit.jupiter.api.Test;

import at.aau.bachelor_project.model.XMLReport;

class JacocoParserTest {

  @Test
  void testJacksonParser() throws IOException {
    var xmlMapper = XmlMapper.builder()
        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        .build();

    var testFile = new File("src/test/resources/jacoco_gui.xml");
    assertTrue(testFile.exists());

    var result = xmlMapper.readValue(testFile, XMLReport.class);

    System.out.println(result);
//    System.out.println(result.getPackages().get(0).getClasses().get(0).getMethods().get(0).getName());
  }

  @Test
  void test() throws IOException {
    var store = new ExecutionDataStore();
    var coverage = new CoverageBuilder();
    var analyzer = new Analyzer(store, coverage);

    var folder = new File("/home/timo/Development/plincs/xport2/COMMONS/target");
    assertTrue(folder.exists());

    analyzer.analyzeAll(folder);
    System.out.println(coverage.getClasses().size());

    for (final IClassCoverage cc : coverage.getClasses()) {
      System.out.printf("Coverage of class %s%n", cc.getName());

      printCounter("instructions", cc.getInstructionCounter());
      printCounter("branches", cc.getBranchCounter());
      printCounter("lines", cc.getLineCounter());
      printCounter("methods", cc.getMethodCounter());
      printCounter("complexity", cc.getComplexityCounter());

      for (int i = cc.getFirstLine(); i <= cc.getLastLine(); i++) {
        System.out.printf("Line %s: %s%n", i,
            getColor(cc.getLine(i).getStatus())
        );
      }


    }
  }

  private void printCounter(final String unit, final ICounter counter) {
    final Integer missed = counter.getMissedCount();
    final Integer total = counter.getTotalCount();
    System.out.printf("%s of %s %s missed%n", missed, total, unit);
  }

}
